# English Wiki Project

Available at Gitlab pages: [https://miraries.gitlab.io/hci-project/](https://miraries.gitlab.io/hci-project/)

## Info

The goal of the project is to perform an analysis of an existing application using the principles defined in the area of Human–computer interaction.  
In this case we used an app developed as part of another class project, and revised it after gaining input from our subjects.  
Unfortunately the project is not available in English.

Other than to complete the primary goal - the class project, the idea was to make use of Vuepress and Gitlab CI/CD to deploy changes to Gitlab Pages automatically on each commit.