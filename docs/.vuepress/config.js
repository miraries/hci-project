module.exports = {
    title: 'HCI Projekat',
    description: 'Analiza Chirp web aplikacije',
    base: "/hci-project/",
    dest: "public",
    serviceWorker: {
        updatePopup: true
    },
    evergreen: true,
    ga: 'UA-140467669-1',
    head: [
        ['link', { rel: 'icon', href: `/logo.png` }],
        ['link', { rel: 'manifest', href: '/manifest.json' }],
        ['meta', { name: 'theme-color', content: '#3eaf7c' }],
        ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
        ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
        ['link', { rel: 'apple-touch-icon', href: `/icons/icon-152x152.png` }],
        ['meta', { name: 'msapplication-TileImage', content: '/icons/icon-144x144.png' }],
        ['meta', { name: 'msapplication-TileColor', content: '#000000' }]
    ],
    themeConfig: {
        lastUpdated: 'Last Updated',
        nav: [
            { text: 'Home', link: '/' },
            { text: 'Repo', link: 'https://gitlab.com/miraries/hci-project' },
        ],
        sidebarDepth: 2,
        displayAllHeaders: true,
        sidebar: [
        	'/',
            '/faza1/',
            '/faza2/',
			'/faza3/',
			'/faza4/',
			'/faza5/',
			'/faza6/',
			'/faza7/',
			'/faza8/',
        ]
    }
}