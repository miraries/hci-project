# Faza 5

## Revizija inicijalnog dizajna

Nakon testiranja sistema sa korisnicima korišćenjem prototipa napravili smo sledeće promjene:

- Popravke na responzivnosti za mobilne uređaje
- Dodat "human-readable" format za datume
- Takođe je omogućeno podešavanje profila

<video width="90%" height="90%" controls autoplay><source src="https://ttprivatenew.s3.amazonaws.com/pulse/miraries/attachments/10776251/TinyTake07-06-2019-11-58-55.mp4" type="video/mp4">Your browser does not support the video tag.</video><br />

Paginacija još uvjek nije implementirana.