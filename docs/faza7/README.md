# Faza 7

## Redizajn interaktivnog prototipa na osnovu rezultata evaluacije
Nakon heurističke evaluacije odlučili smo da unesemo sljedeće promijene:

- Obavještenje o svim akcijama
- Pregled chirpova od strane neregistrovanih korisnika
- Link koji vodi na stranicu za pomoć

## Druga verzija interaktivnog prototipa sistema

<video width="90%" height="90%" controls autoplay><source src="https://ttprivatenew.s3.amazonaws.com/pulse/miraries/attachments/10776217/TinyTake07-06-2019-11-46-01.mp4" type="video/mp4">Your browser does not support the video tag.</video><br />

![Pomoć](./help.png)

## Drugo testiranje sistema sa korisnicima

Testiranje su izvali isti korisnici sa dodatnim zadatkom.

### Zadatak #1
Registrovani ste kao korisnik. Nađite postove i privatne poruke razmijenjene sa korisnikom koji je zadnji odgovorio na neki od vaših postova.

### Zadatak #2
Registrujte se, nađite interesantne postove pretragom i lajkujte ih

### Zadatak #3
Ulogujte se i izmijenite šifru vašeg profila kao i neke osnovne podatke.

## Analiza i zaključak
Zaključili smo da su, osim paginacije koju nismo ni odlučili da implemenitramo u ovom trenutku, svi problemi ispravljeni.  
Korisnici nisu imali većih primjedbi na sistem.
