# Faza 1 

## Kratak opis sistema
**Chirp** je društvena mreža stvorena po ideji tvitera, ali sa većim fokusom na duže tekstove tj. blogove. Ovo bi, idealno, dalo više mjesta kvalitetnim tekstovima i lako izlaganje korisnica tekstovima drugih korisnika koje žele da prate. Takođe nema ograničenja na dužinu komentara tako da su korisnici u mogućnosti da daju više konstruktivne komentare na radove drugih korisnik. Pored toga nudi sve standardne funkcije društvene mreže kao što su privatne poruke, lajkovi, avatari i sl. Kako je front-end aplikacije zasnovan na Vue.js framework-u, tj. nekoliko komponenata koje se ponavljaju na više stranica i u suštini se ponašaju isto, očekujemo da će korisnici ubrzo postati vješti u korištenju aplikacije.

## Projektni tim
Ivan Kotlaja [encore.ivan@gmail.com](mailto:encore.ivan@gmail.com)