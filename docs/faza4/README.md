# Faza 4

## Lo-fi prototip sistema
![Sketch](./sketch.png)
![Sketch Data](./sketch-data.png)

## Testiranje sistema sa izabranim korisnicima

### Korisnici

#### Korisnik 1
- Pol: Muški
- Godine: 21
- Nivo edukacije: Student
- Korišćenje mreža: Slabo
- Poznavanje ICT: Loše

#### Korisnik 2
- Pol: Muški
- Godine: 22
- Nivo edukacije: Student
- Korišćenje mreža: Odlično
- Poznavanje ICT: Ispod prosjeka

#### Korisnik 3
- Pol: Muški
- Godine: 28
- Nivo edukacije: Student
- Korišćenje mreža: Ispod prosjeka
- Poznavanje ICT: Vrlo dobro

### Metod testiranja

Korisnici su individualno testirali platformu putem web aplikacije, i računarom i telefonom. Uočene su neke greške u dizajnu prilikom interfejsa za mobilne uređaje. Tražen je komentar za svaku akciju i njenu bitnost i lakoću korišćenja.

#### Zadatak #1
Registrovani ste kao korisnik. Nađite postove i privatne poruke razmijenjene sa korisnikom koji je zadnji odgovorio na neki od vaših postova.

#### Zadatak #2
Registrujte se, nađite interesantne postove pretragom i lajkujte ih


### Zaključak
Nakon analize načina na koji su korisnici koristili sistem našli smo neke greške u dizajnu našeg lo-fi prototipa. Takođe smo odlučili da odstranimo neke funkcionalnosti za sad za lakše razumijevanje sistema. Odlučili smo da dodamo neke detaljnije i jasnije indikatore o datumima. I jedan od većih propusta je nedostatak paginacije što usporava i otežava rad sa platformom.