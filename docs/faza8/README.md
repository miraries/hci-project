# Faza 8
## Končan dizajn korisničkog interfejsa interaktivnog sistema

Konačan dizajn nije drastično različit ali funkcionalosti koje nudi su veće, npr. u dizajnu može biti razlika samo jedno dugme, ali ono vodi na pomoć koje nudi detaljan opis sistema i česta pitanja. Pored svega sistem je ostao vrlo minimalističan sa brzim animacijma što se tiče dizajna - što je bio i cilj. Mada je treba još dosta doraditi i pokriti još neke slučajeve korišćenja platforma se definitivno može već koristiti.


## Kratak pregled svih faza dizajna (1-7)

1. Definisanje projekta i učesnika u testiranju
2. Predstavljene persone i urađeni upitnici kako bi dobili dobar uzorak korisnika sistema kao i zadaci.
3. Predstavljeni scenariji korišćenja sistema kao i komparacija sa Twitter aplikacijom. Inicijalna skica i analiza.
4. Razvijen Lo-fi prototip sistema koji je predstavljen korisnicima kako bi se validirao.
5. Revizija inicijalnog dizajna i razvijanje interaktivnog prototipa.
6. Heuristička evaluacija sistema gdje su primijećeni određeni nedostaci.
7. Na osnovu prethodne faze napravljene su promjene i funkcionalnosti.