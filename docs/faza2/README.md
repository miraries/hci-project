# Faza 2 

## Definisanje persona i njihovih ciljeva

### Opis persona

#### Danilo Miranović
---

Danilo ima 21 godinu i student je elektrotehničkog fakulteta na Univerzitetu Crne Gore. Danilova velika strast su integrali. Danilo kao i većina nas koristi društvene mreže, doduše umjereno.

##### Ciljevi
* Završi fakultet
* Pođe više da radi na ljeto

##### Opravdanje

Danilo je prosječan korisnik ICT-a. Mada Danilo trenutno nije korisnik mreža sličnim tviteru, ne znači da nije bio. Smatram da će on biti dobar primjer budućih korisnika sistema koji nemaju pozadinu u ICT svijetu.

#### Miloš Popović
---

Miloš ima 21 godinu i student je prirodno-matematičkog fakulteta na Univerzitetu Crne Gore. Miloševa velika strast su takođe integrali, kao i kompjuterske igre. Miloš je aktivan korisnik više društvenih mreža.

##### Ciljevi
* Završi fakultet
* Pođe više da radi na ljeto

##### Opravdanje

Miloš je više nego prosječan korisnik ICT-a. Kao aktivni korisnik facebook-a, tvitera, snapchata i instagrama mislim da može dati dobro mišljenje o nedostacima aplikacije. Smatram da će on biti dobar primjer budućih korisnika sistema koji su već navikli na postojeće mreže.


#### Spasoje Đurović
---
Spasoje ima nepoznat broj godina i student je fakulteta informacionih tehnologija na Univerzitetu Mediteran. Spasojeve strasti su muzika, programiranje, i teretana. Spasoje je manje aktivan korisnik društvenih mreža ali često čita članke o programiranju.

##### Ciljevi
* Nastavi studiranje
* Nađe posao u struci

##### Opravdanje

Spasoje ima pozadinu iz ICT-a. Iako rijetko koristi društvene mreže može dati uvid u to koliko bi bio zanteresovan da počne da koristi ovakvu. Smatram da će on biti dobar primjer budućih korisnika sistema ne koriste društvene mreže.

## Intervju - pitanja i odgovori

1. Da li često koristite socijalne mreže?
  * Danilo: Da
  * Miloš: Da
  * Spasoje: Ne baš

2. Da li ste zadovoljni trenutnom ponudom socijalnih mreža?
  * Danilo: Ne
  * Miloš: Relativno da
  * Spasoje: Ne

3. Da li ste imali iskustva sa "user-owned" alternativama kao Diaspora/Mastodon?
  * Danilo: Ne
  * Miloš: Ne 
  * Spasoje: Ne, čuo

4. Da li ste koristili "publishing" platforme kao Medium za čitanje blogova?
  * Danilo: Ne
  * Miloš: Ne
  * Spasoje: Da

5. Koliko često postavljate sadržaj na socijalnim mrežama?
  * Danilo: Rijetko
  * Miloš: Redovno
  * Spasoje: Nikad

6. Da li smatrate da na socijalnoj mreži ima mjesta za konstruktivan razgovor i postove sa više značenja?
  * Danilo: Na trenutnim ne
  * Miloš: Na trenutnim ne
  * Spasoje: Na trenutnim ne

7. Da li biste koristili mrežu koja podstiče takav sadržaj?
  * Danilo: Da
  * Miloš: Vjerovatno
  * Spasoje: Da

8. Koliko vam je bitna anonimnost?
  * Danilo: Relativno da
  * Miloš: Nije
  * Spasoje: Relativno da

8. Koliko vam je bitno slanje privatnih poruka na još jednoj mreži?
  * Danilo: Nije
  * Miloš: Da
  * Spasoje: Nema mišljenje

## Opis ključnih zadataka

- Kreiranje platforme koja:
  - će podsticati razmjenu konstruktivnog i kvalitetnog sadržaja
  - će dozovljavati, ili bar ne ometati anonimnost korisnika (tzv. throw-away accounts)
  - će biti jednostavna za korištenje i nalaženje relevantnog sadržaja
- Korisnici mogu da:
  - kreiraju postove
  - zapraćuju druge korisnike
  - "lajkuju" postove kao
  - šalju privatne poruke

## Zadaci
|                            | Danilo | Miloš | Spasoje |
|----------------------------|--------|-------|---------|
| Postavljanje konstruktivnog sadržaja      | 5      | 3     | 5       |
| Postavljanje anonimnog sadržaja       | 4      | 1     | 4       |
| Napredna pretraga sadržaja | 3      | 4     | 5       |
| Slanje privatnih poruka    | 1      | 5     | 3       |
| Minimalan interfejs      | 5      | 5     | 4       |

*1 - Nepotrebno*  
*5 - Neophodno*

## Zaključak

Mada je veličina uzorka mala smatram da se neke stvari definitivno mogu generalizovati. Kao što možemo vidjeti korisnici nisu upoznati sa alternativama vodećim socijalnim mrežama, i popularizacija same činjenice da alternative postoje bi bio jedan od glavnih ciljeva. Zbog toga je vrlo bitno napraviti platformu koja je čak i jednostavnija za korištenje nego trenutne vodeće.  
Takođe možemo vidjeti da je korisnicima bitna mogućnost da ostanu anonimni prilikom rada korištenja. Mada nećemo implementirati posebne funkcionalnosti za to, nećemo razvijati funkcionalnosti koje to ograničavaju.  
Nalaženje sadržaja relevantnog korisniku je vrlo bitna stvar, posebno u prvih par susreta sa mrežom jer ako korisnik ne nađe dovoljno interesantnih i relevantnih stvari neće se zadržati na platformi.  
Za privatne poruke nemamo definitivan odgovor i mada bismo htjeli da platforma bude vrlo fokusirana na par zadataka, privatne poruke bi ipak bile jedini način direktnog kontaktiranja korisnika koji nemaju drugi kontakt tako da moramo omogućiti tu funkcionalnost.

Nismo pominjali nativne mobilne aplikacija kako trenutno nisu u planu, web platforma u skladu sa PWA funkcionalnostima funkcioniše za sve uređaje.