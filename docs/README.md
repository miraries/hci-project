---
home: true
heroImage: /hero.png
actionText: Započni →
actionLink: /faza1/ 
features:
- title: Simplicity First
  details: Chirp se sastoji od par komponenata i nudi jednostavan i minimalističan pregled sadržaja
- title: Laravel & Vue-Powered
  details: Napravljen korišćenjem najbolje prakse, razvijanjem vođenim testovima
- title: Performant
  details: Kako je razvijena kao Single Page Application nakon učitavanja stranice aplikacija radi izuzetno, učitavajući samo sadržaj
footer: MIT Licensed | Copyright © 2019-present Ivan Kotlaja
---