# Faza 3 

## Scenariji upotrebe

- Spasoje je napravio osnovne funkcionalnosti svoje aplikacije, međutim ima poteškoća sa korišćenjem nekih API servisa. Međutim aplikacija je dovoljno funkcionalna da je može dati na testiranje ljudima.
  - **I Scenario**  
 	Spasoje će ući na mrežu, i kako je uspio da nađe ljude relevantne sebi, (u ovom slučaju programere) on će postaviti svoj problem kao post i eventalno tagovi neke konkretne osobe koje mu mogu biti od pomoći. Kako Spasoje nema ogranicenje za dužinu posta, kao ni za slike* može vrlo lako opisati problem, a iz istog razloga mu drugi korisnici mogu dati konstruktivnu pomoć
  - **II Scenario**  
 	Kao što smo pomenuli i pored poteškoća Spasoje može testirati aplikaciju ali su mu neophodni ljudi za to. Kako Spasoja prate i ljudi relevantni iz drugih tema on može podijeliti testnu verziju aplikacije i u detalje napisati oko čega mu treba mišljenje, nakon čega drugi mogu odgovoriti sa svojim ili podijeliti njegov zahtjev svojim prijateljima.

- **III Scenario**  
	Danilo je završio bitan programerski projekat koji koristi najonovije tehnologije i želi da podijeli šta je naučio i koje su njihove mogućnosti sa svojim prijateljima i pratiocima. Umjesto da traži gdje bi mogao postaviti svoj poduži tekst, uzima hosting za svoj blog, radi sa bazama i slično, on može direktno podijeliti svoj tekst, dobiti konstruktivnu podršku kao i podijeliti svoje znanje na još više ljudi, i to sve na našoj platformi. 

## Komparativna analiza

Direktna konkurencija je očigledno Twitter, tako da smo ga koristili za poređenje.

| Parametri           | Ocjena |
|---------------------|--------|
| Ograničenje postova | 1      |
| Dizajn              | 4      |
| Pretraga            | 4      |
| Anonimnost          | 2      | 
| API                 | 2      |

Kao što smo naveli već par puta, Twitter ograničava realizaciju kontruktivne diskusije. Rezultat toga je teška navigacija sajta i potreba za alatima koji razdvjaju postove na više tvitova:
![Twitter Split Post](./twitter_split.png)


Mada Twitter ne zahtijeva broj telefona za registraciju, ipak će korisnike da napominje da povežu telefon, mail i sl. sa nalogom što bi narušilo njihovu anonimnost.
API za aplikacije trećih strana je vrlo diskutabilan što se tiče nekih ograničenja, ali je ipak mnogo bolji nego druge mreže.

## Incijalni dizajn sistema

![Mapa uma](./mindmap.png)

## Prikaz (slke skica) inicijalnog dizajna sistema

![Mapa uma](./sketch_init.png)

## Analiza predloga (skica) i zaključak

Kako tok akcija nije obavezno linearan, umjesto scenaria analiziraćemo skice pojedinačno. Smatramo da je bitno da sistem bude vrlo jednostavan kako bi korisnici lako mogli da se prilagode i prave sadržaj. 