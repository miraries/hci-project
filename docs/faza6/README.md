# Faza 6

## Heuristička evaluacija
### 1. Vidljivost statusa sistema

Aplikacija ima naziv i indikacije na svakojs stranici kao i logo kojim se korisnik može uvjek vratiti na početnu stranicu tako da korisnici nemaju yna;ajnih problema sa navigacijom.

### 2. Sličnost sistema sa stvarnim svijetom

Osim samog naziva Chirp, sistem je sličan Twitteru sa čim je većina korisnika upoznata. Aplikacija podržava real-time prevođenje u druge jezike na front-endu tako da, ukoliko je jezik podržan, i taj problem je riješen.

### 3. Kontrola i upravljanje

Platformom je jednostavno rukovati kako je dizajniran korišćenjem komponenti koje su se koristile više puta na razćičitim stranicama, što znači da iako korisnik nije ranije koristio neku funkcionalnost njeni djelovi su mu već poznati i nije mu problem privići se na nove funkcionalnosti.

### 4. Konzistentnost i standardi

Kao što je već pomenuto zbog dizajna sistema korišćenjem komponenti dizajn je vrlo konzistentan. Takođe su korištene odgovarajuće ikonice za pojedine akcije. Korišten je i Gravatar API kako bi korisnici odmah pri registraciji imali avatar postavljen na toj platformi. Osim paginacije svi glavni standardi i u dizajnu i sigurnosti su ispoštovani.

### 5. Prevencija grešaka

U sistemu je implementiran sistem notifikacija o svakoj akciji koju korisnik izvede. Na primer, ako učita chirpove nekog korisnika vidjeće da se ta akcija pokrenula i da se izvršava, isto važi za postavljanje i brisanje drugih modela. Elementi se disable-uju po potrebi tako da korisnik ne može voljno izvesti nedozvoljene akcije.

### 6. Prepoznavanje prije nego prisjećanje

Isto važi kao za prethodne (korišćenje komponenti)

### 7. Fleksibilnost i efikasnost upotrebe

Ovdje je paginacija glavni problem jer pregled bez te funkcionalnosti nije idealan. Ovo se može vidjeti pri učitavanju chirpova i privatnih poruka.

### 8. Poruke o greškama

Ukoliko dođe do greške na svakoj formi je dodata provjera i na front i back-endu kao i poruka za svako polje koje nije u redu. Ukoliko dođe do greške na serveru (npr. status kod 500) korinsik je obaviješten o tome sa odgovarajućom porukom koja je takođe prevedena na odgovarajućem jeziku.

### 9. Minimalistički dizajn

Dizajn je, može se primijetit vrlo minimalističan, kao i paleta boja. Vrlo je fokusiran i koristi vibrantne boje samo po potrebi.

### 10. Omogućiti pomoć - help

Dat je "feedback"  za večinu akcija kao i animacije za lakšu upotrebu ali nije dat način ili dokumentacija.

## Analiza evaluacije i zaključak
Aplikacija zadovoljava većinu kriterijuma, a za ispravku onih koje ne zadovoljava nisu neophodne prevelike izmjene.